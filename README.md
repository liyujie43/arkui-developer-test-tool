# ArkUI开发者自测试小工具

## 介绍

帮助`ArkUI`的部分CPP开发者快速方便地验证测试套

## 安装教程

- `python >= 3.7.9`
- 编译测试套的服务器或wsl必须要有`lcov`分支覆盖率工具
- 目前测试支持`rk3568`板子，其余的设备有待支持（PS:主要是代码写的比较乱）
- 安装彩色日志`colorlog`和ssh连接库`paramiko`

安装`lcov`分支覆盖率工具
```shell
sudo apt-get install lcov
```

安装`python`依赖库
```bash
pip install colorlog
pip install paramiko
```

## 使用说明

### 关于config.json文件的配置说明

> 其中`personal`部分属于个人配置部分

- `code-path`: 代码路径(例如: `/home/user/openharmony`)
- `code-gcov`: `llvm-gcov.sh`文件的放置路径(例如: `/home/user/openharmony/tools/llvm-gcov.sh`)
- 在任意目录（必须是服务器或`wsl`上的路径）创建`llvm-gcov.sh`, 其中的具体内容为

```shell
exec code-path/prebuilts/clang/ohos/linux-x86_64/llvm/bin/llvm-cov gcov "$@"
```

- 然后给`llvm-gcov.sh`赋可执行权限

```shell
chmod u+x llvm-gcov.sh
```

> 其中`subsystem`部分属于子系统部分

- `test_paths`和`shiled_paths`: 不做修改, 是预留项
- `coverage_config`: `arkui`的覆盖版本编译参数
- `coverage_path`: 属于`gcda`产生后覆盖的路径, `arkui`的路径是`code-path/out/rk3568/obj/foundation/arkui/ace_engine`
- `test_target_name`: 子系统编译测试目标名称, `arkui`的是`ace_engine_test`
- `test_out_path`: 子系统编译后产生测试套的目录, `arkui`的路径是`code-path/out/rk3568/tests/unittest/ace_engine`
- `build_target`: 子系统相关`so`的编译目标, arkui的是ace_packages
- `out_path`: 子系统相关`so`的产生路径, 可以是多个, arkui可以只推送这一个`libace_compatible.z.so`
- `push_path`: 子系统推入设备的路径，arkui在rk3568上是`/system/lib/platformsdk`

> 执行参数

- `-b` `--build-target`: 测试套目标, 可以是多个目标, 例如 --build-target list_test_ng stack_test_ng, 用空格隔开
- `-f` `--fast-rebuild`: 是否快速编译
- `-c` `--coverage`: 是否产生覆盖率文件
- `-p` `--push-so`: 是否推送so

> 执行例子

```python
python run.py --build-target list_test_ng --fast-rebuild --coverage
# or
python run.py -b list_test_ng -f -c
```
注意：--build-target是必填项，其余都是选填
