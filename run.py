import argparse
from utils import parse_json
from utils import CommandExecute
from build import BuildCodeHandler
from execute import ExecuteTestsHandler


parser = argparse.ArgumentParser()
parser.add_argument("-b", "--build-target", nargs='+', type=str, default=None)
parser.add_argument("-f", "--fast-rebuild", action="store_true")
parser.add_argument("-c", "--coverage", action="store_true")
parser.add_argument("-p", "--push-so", action="store_true")
config = parse_json('config.json')
host = config['personal']['ssh']['host']
port = config['personal']['ssh']['port']
username = config['personal']['ssh']['username']
password = config['personal']['ssh']['password']
command_execute = CommandExecute(host, port, username, password)
args = parser.parse_args()
targets = args.build_target
fast_rebuild = args.fast_rebuild
coverage = args.coverage
push_so = args.push_so
build_code_handler = BuildCodeHandler(command_execute, config)
build_code_handler.process(targets, fast_rebuild, coverage, push_so)
execute_tests_handler = ExecuteTestsHandler(command_execute, config, coverage)
execute_tests_handler.process()
